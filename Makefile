ADDON_DIR=$(HOME)/.local/share/Anki2/addons21

test:
	ln -s $(shell pwd)/japanese-note-autogenerator $(ADDON_DIR)

install:
	cp ./japanese-note-autogenerator $(ADDON_DIR)/japanese-note-autogenerator

package:
	rm -rf ./japanese-note-autogenerator/__pycache__
	zip -r package.zip ./japanese-note-autogenerator/
	mv package.zip package.ankiaddon