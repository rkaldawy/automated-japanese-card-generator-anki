# Japanese Note Auto-Generator for Anki

This is an addon for Anki which auto-generates Japanese notecards. It takes a list of desired Japanese vocabulary and uses the Jisho.org API to create corresponding notecards.

Notecards include:

* A list of **definitions** and corresponding grammatical **parts of speech**.
* **JLPT Level**.
* **Vocabulary reading** in hiragana.
* A list of **alternate readings** for the vocabulary word.
* **Kanji explanations** for each kanji character in the vocabulary word.

### Installation

The easiest way to install this addon is through the official Anki addon page; see [here](https://ankiweb.net/shared/info/611151309).

To install the addon manually, clone this project and copy the `japanese-note-autogenerator` subdirectory to your local Anki desktop's addon folder. Restart Anki if it is running.

### How to use

Upon (re)starting, the `Tools` menu on the Anki main window will have a new option to auto-generate Japanese notes. Selecting this option will provide a menu; enter your desired vocabulary words in the menu, and the addon will generate corresponding notes and place them in a custom deck for review. Each vocabulary word must be entered in a separate line in the menu. 

![Tool in action](https://media.giphy.com/media/iMClgl3GkrjKCYlCFo/giphy.gif)

### Future additions

- [] Add example sentences for vocabulary
- [] Add audio samples for vocabulary
- [] Add a Chrome extension for one-click note creation.
