""" Methods for querying Jisho for vocabulary information. 
"""

import requests
import json
import re
from bs4 import BeautifulSoup

from aqt.utils import showInfo, askUser, showWarning

API_QUERY = "https://jisho.org/api/v1/search/words?keyword={}"
KANJI_QUERY = "https://kanjiapi.dev/v1/kanji/{}"
WEB_QUERY = "https://jisho.org/search/{}"
SENTENCE_QUERY = "https://jisho.org/search/{}%20%23sentences"
MANUAL_LINK = "https://jisho.org/word/{}"

def find_kanji(word):
    return re.findall("[\u4e00-\u9FFF]", word)

def parse_example_sentences(word):
    query = SENTENCE_QUERY.format(word)
    page = requests.get(query)
    soup = BeautifulSoup(page.content, 'html.parser')

    ret = []
    sentences = soup.find_all(class_='entry sentence clearfix')
    for sentence in sentences:
        segments = sentence.find_all(lambda tag: tag.get('class') == ['clearfix'])
        furiganas, words = [], []
        for segment in segments:
            furigana = segment.find(class_='furigana')
            if furigana:
                furigana = furigana.get_text()
            furiganas.append(furigana)
            word = segment.find(class_='unlinked').get_text()
            words.append(word)        
        en = sentence.find(lambda tag: tag.get('class') == ['english']).get_text()

        ret.append({'furigana': furiganas, 'words': words, 'english': en})

    return ret
        
def parse_furigana(word):
    query = WEB_QUERY.format(word)
    page = requests.get(query)
    soup = BeautifulSoup(page.content, 'html.parser')

    furigana = soup.find(class_='furigana').find_all('span')
    pattern = r'<span.*>(.*)</span>'
    furigana = [re.findall(pattern, str(elt))[0] for elt in furigana]

    return furigana


""" Queries the jisho API with a word to define. Returns a dictionary with word definiton and 
    information. Only returns the first result for the word.
"""
def jisho_query_word(word):
    query = API_QUERY.format(word)
    r = requests.get(query)
    data = json.loads(r.text)['data']
    if not data:
        return None
    data = data[0]

    ret = {}

    # Vocabulary readings
    readings = []
    for elt in data['japanese']:
        if 'reading' not in elt:
            continue
        reading = elt['reading']
        if 'word' not in elt:
            word = reading
        else:
            word = elt['word']
        readings.append({'word': word, 'reading': reading})
    if not readings:
        return None

    ret['word'] = readings[0]['word']
    ret['reading'] = readings[0]['reading']
    ret['alternate_readings'] = readings[1:]

    # Replace word with Jisho's primary reading for further Jisho queries.
    word = ret['word']

    # JLPT Level
    try:
        ret['jlpt'] = 'N' + data['jlpt'][0].split('jlpt-n')[1]
    except:
        ret['jlpt'] = 'n/a'

    # Definitions of the word
    definitions = []
    for elt in data['senses']:
        definitions.append({'definition': elt['english_definitions'], 'parts_of_speech': elt['parts_of_speech']})
    ret['definitions'] = definitions

    # Jisho link
    ret['link'] = MANUAL_LINK.format(data['slug'])

    # Kanji breakdown
    kanji_infos = []
    for k in find_kanji(ret['word']):
        query = KANJI_QUERY.format(k)
        r = requests.get(query)
        data = json.loads(r.text)
        kanji_info = {'kanji': data['kanji'], 'definitions': data['meanings']}
        kanji_infos.append(kanji_info)
    ret['kanjis'] = kanji_infos

    # Furigana
    ret['furigana'] = parse_furigana(word)

    # Example sentences
    ret['example_sentences'] = parse_example_sentences(word)
    
    return ret