""" Methods for creating auto-generated Japanese notes.
"""
from .jisho import jisho_query_word 
from collections import defaultdict
from aqt.utils import showInfo, askUser, showWarning

from xml.etree import ElementTree as ET

def html_element(type_, class_, parent=None):
    element = ET.Element(type_, attrib={'class': class_})
    if parent:
        parent.append(element)
    return element

""" Creates a note. Some fields have HTML/CSS styling baked in to represent lists, line breaks, 
    etc.
"""
def create_note(mw, word):
    r = jisho_query_word(word)
    if not r:
        return None

    n = mw.col.newNote()
    n._fmap = defaultdict(str, n._fmap)

    n['Word'] = r['word']
    n['Reading'] = r['reading']

    n['JLPT'] = r['jlpt']
    n['Jisho Link'] = r['link']

    template = """
    <div>
    <div style='font-size: 11px; color: DarkGray;'>
        {}
    </div>
    <span style='font-size: 24px; color: DarkGray;'> 
        {}. 
    </span> 
    <span style='font-size: 24px;'> 
        {} 
    </span>
    </div>
    <br>
    """

    if not r['definitions']:
        return None
    definitions = ""
    for idx, definition in enumerate(r['definitions']):
        d = "; ".join(definition['definition'])
        pos = ", ".join(definition['parts_of_speech'])
        definitions += template.format(pos, idx+1, d)
    n['Definitions'] = definitions

    template = [
    """
    <div style='font-size: 11px; color: DarkGray;'>
        Other forms: 
    </div>
    <div style='font-size: 24px;'> 
        {} 
    </div> 
    """,
    """
        {}  ({})
    """]

    readings = []
    for elt in r['alternate_readings']:
        readings.append(template[1].format(elt['word'], elt['reading']))
    readings = ", ".join(readings)
    if not readings:
        readings = "None"
    readings = template[0].format(readings)
    n['Alternate Readings'] = readings

    template = [ \
    """
    <div style='font-size: 11px; color: DarkGray;'>
        Kanji breakdown: 
    </div>
    {}
    """, \
    """
    <div>
        {}: {}
    </div>
    """] 

    kanji_breakdown = ""
    for kanji in r['kanjis']:
        k = kanji['kanji']
        d = "; ".join(kanji['definitions'])
        kanji_breakdown += template[1].format(k, d)
    if not kanji_breakdown:
        kanji_breakdown = "N/A"
    kanji_breakdown = template[0].format(kanji_breakdown)
    n['Kanji Meanings'] = kanji_breakdown

    template = """
    <span class='furigana'>
        {}
    </span>
    """

    furigana = ""
    for elt in r['furigana']:
        furigana += template.format(elt)
    n['Furigana'] = furigana

    template = [ \
    """
        <ul class='example_sentence box'>
            {}
        </ul>
        <div class='entry'>
            {}
        </div>
        <br>
    """, \
    """
    <li class='example_sentence clearfix'>
        <span class='example_sentence furigana'>
            {}
        </span>
        <span>
            {}
        </span>
    </li>
    """]

    sentences = ""
    for s in r['example_sentences']:
        sentence = ""
        for f, w in zip(s['furigana'], s['words']):
            if not f:
                f = ""
            sentence += template[1].format(f, w)
        sentences += template[0].format(sentence, s['english'])
    n['Example Sentences'] = sentences

    n.addTag('japanese_automated')

    return n